#include <iostream>

const int max = 10;
int arr[max];

void main()
{
    for (int i = 0; i < max; i++)
    {
        arr[i] = i;
    }

    for (int i : arr)
    {
        std::cout << arr[i] << '\n';
    }

    std::cout << "size of array is: " << sizeof(arr) << '\n';
}