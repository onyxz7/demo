#include <iostream>
#include <ostream>

using namespace std;

void referenceExample()
{
    int studentNumber = 21;
    int& classNumber = studentNumber;
    int pearsonNumber = studentNumber;

    cout << classNumber << '\n' << studentNumber << '\n' << &studentNumber << '\n' << &classNumber << '\n' << &
        pearsonNumber << '\n';
}

void pointerExample()
{
    string food = "Pizza"; // A food variable of type string
    string* ptr = &food; // A pointer variable, with the name ptr, that stores the address of food

    // Output the value of food (Pizza)
    cout << food << "\n";

    // Output the memory address of food (0x6dfed4)
    cout << &food << "\n";

    // Output the memory address of food with the pointer (0x6dfed4)
    cout << ptr << "\n";

    cout << *ptr << "\n";
}

void main()
{
    referenceExample();
    pointerExample();
}
