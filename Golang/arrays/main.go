package main
import ("fmt")

func main() {
	phones := [...]string{"Google Pixels", "Apple iPhone", "Samsung Galaxy"} // lenght is inferred
	fmt.Print(phones)
	fmt.Println("")
	fmt.Println("Some phones :", phones)
	fmt.Print("Only the second one in the list : " + phones[1] + "\n")

	// another list of 6
	var numbers = [5]float64{0:45.5, 1:879.98, 2:852.741, 4:23.31} // partialiy initialise (slot 4 not defined)
	fmt.Println(numbers)
	numbers[3] = 234.2
	fmt.Println("After changing \"numbers[4]\" to", numbers[3], ":\n", numbers)

	// print lenght of phones && numbers
	fmt.Println(len(phones))
	fmt.Println(len(numbers))
}
