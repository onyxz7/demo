package main

import "fmt"

func main() {
	students := []string{"Marc", "Antoine", "Julien"}
	for index, name := range students {
		id := index + 1
		fmt.Printf("Student %d : %s\n", id, name)
	}
}
