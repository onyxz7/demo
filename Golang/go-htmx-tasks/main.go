package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"
	"sync"
)

type Task struct {
    ID        int
    Content   string
    Completed bool
}

var (
    tasks      []Task
    tasksMutex sync.Mutex
    nextID     int = 1
    tmpl       *template.Template
)

func init() {
    var err error
    tmpl, err = template.ParseFiles("index.html")
    if err != nil {
        panic(err)
    }
}

func main() {
    http.HandleFunc("/", handleIndex)
    http.HandleFunc("/add-task", handleAddTask)
    http.HandleFunc("/toggle-task", handleToggleTask)
    http.HandleFunc("/delete-task", handleDeleteTask)

    fmt.Println("Server started on http://localhost:8080")
    http.ListenAndServe(":8080", nil)
}

func handleIndex(w http.ResponseWriter, r *http.Request) {
    tasksMutex.Lock()
    defer tasksMutex.Unlock()
    tmpl.Execute(w, tasks)
}

func handleAddTask(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
        taskContent := r.FormValue("content")
        tasksMutex.Lock()
        newTask := Task{ID: nextID, Content: taskContent, Completed: false}
        tasks = append(tasks, newTask)

		fmt.Printf("Task with ID %d added\n", nextID)

        nextID++
        tasksMutex.Unlock()

        tmpl.ExecuteTemplate(w, "task-list", tasks)
    }
}

func handleToggleTask(w http.ResponseWriter, r *http.Request) {
    idStr := r.FormValue("id")
    id, err := strconv.Atoi(idStr)
    if err != nil {
        http.Error(w, "Invalid task ID", http.StatusBadRequest)
        return
    }

    tasksMutex.Lock()
    for i := range tasks {
        if tasks[i].ID == id {
            tasks[i].Completed = !tasks[i].Completed
            break
        }
    }
    tasksMutex.Unlock()

    tmpl.ExecuteTemplate(w, "task-list", tasks)
}

func handleDeleteTask(w http.ResponseWriter, r *http.Request) {
    idStr := r.FormValue("id")
    id, err := strconv.Atoi(idStr)
    if err != nil {
        http.Error(w, "Invalid task ID", http.StatusBadRequest)
        return
    }

    fmt.Printf("Attempting to delete task with ID: %d\n", id)

    tasksMutex.Lock()
    for i := range tasks {
        if tasks[i].ID == id {
            tasks = append(tasks[:i], tasks[i+1:]...)
            fmt.Printf("Task with ID %d deleted\n", id)
            break
        }
    }
    tasksMutex.Unlock()

    tmpl.ExecuteTemplate(w, "task-list", tasks)
}
