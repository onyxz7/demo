package main

import (
	"fmt"
	"math/rand"
	"strings"
)

const (
	MinNumber = 1
	MaxNumber = 100
)

func main() {
	var awnser int = rand.Intn(100) 
	fmt.Printf("%d is the awnser", awnser)
	var gameRunning bool = true;
	for gameRunning {
		var userTry int
		fmt.Print("\nEnter a guess : ")
		fmt.Scan(&userTry)
		isNumberValid, errors := isNumberValid(userTry)
		if(!isNumberValid) {
			fmt.Print(strings.Join(errors, "\n"))
		}
		if(userTry == awnser) {
			fmt.Printf("\nYou found the awnser wich was %d", awnser)
			gameRunning = false
		} else {
			if (userTry > awnser) {
				fmt.Print("\nToo high")
			} else if (userTry < awnser) {
				fmt.Print("\nToo low")
			}
		}
	}
}

func isNumberValid(number int) (bool, []string) {
	isValid := true
	errors := []string{}
	if (number < MinNumber) {
		errors = append(errors, fmt.Sprintf("The number should be higher than %d", MinNumber))
	}
	if (number > MaxNumber) {
		errors = append(errors, fmt.Sprintf("The number should be lower than %d", MaxNumber))
	}
	return isValid, errors
}