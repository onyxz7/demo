package main

import (
	"fmt"
	"html/template"
	"net/http"
)

const (
	httpPort = ":8080"
)

var (
	tmpl *template.Template
)

func init() {
	var err error
	tmpl, err = template.ParseFiles("pages/index.html")
	if err != nil {
		panic(err)
	}
}

func main() {
	http.HandleFunc("/", handleIndex)

	fmt.Println("Server started on http://localhost" + httpPort)
	http.ListenAndServe(httpPort, nil)
}

func handleIndex(wr http.ResponseWriter, r *http.Request) {
	tmpl.Execute(wr, nil)
}
