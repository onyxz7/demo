package main
import ("fmt")

const PI float64 = 3.141592

func main() {
	//declare global vars of main func
	var choice int
	var radius float64
	var number1_for_average, number2_for_average, number3_for_average float64
	var price float64
	var number1ToBigger, number2ToBigger float64
	var saleAmount float64
	var numberToSumSquare int

	// Main Console
	fmt.Println("What do you want to do ?")
	fmt.Println("	1) Radius to perimeter.")
	fmt.Println("	2) 3 numbers to average.")
	fmt.Println("	3) Price to total with taxes.")
	fmt.Println("	4) Wich number is bigger.")
	fmt.Println("	5) What salary for a given sale amount.")
	fmt.Println("	6) What salary for a given sale amount.")
	fmt.Println("	7) num1 * num2.")
	fmt.Println("	8) num ^ num2.")
	fmt.Println("")
	fmt.Scanln(&choice)
	fmt.Println("")

	if choice == 1 {
		fmt.Println("Enter a radius to get the circle perimeter : ")
		fmt.Scanln(&radius)
		fmt.Println("The perimeter is : ", radiusToPerimeter(radius))
	} else if choice == 2 {
		fmt.Println("Enter 3 numbers to get there average : ")
		fmt.Scanln(&number1_for_average)
		fmt.Scanln(&number2_for_average)
		fmt.Scanln(&number3_for_average)
		fmt.Println("The average is : ", number123ToAverage(number1_for_average, number2_for_average, number3_for_average))
	} else if choice == 3 {
		fmt.Println("Enter a price to got the total with the taxes : ")
		fmt.Scanln(&price)
		fmt.Println("The total is : ", priceToTotalWithTaxes(price))
	} else if choice == 4 { 
		fmt.Println("Enter 2 numbers to know the bigger one : ")
		fmt.Scanln(&number1ToBigger)
		fmt.Scanln(&number1ToBigger)
		fmt.Println("The bigger number is : ", numberThatIsBigger(number1ToBigger, number2ToBigger))
	} else if choice == 5 {
		fmt.Println("Enter a saleAmount to get salary : ")
		fmt.Scanln(&saleAmount)
		fmt.Println("The salary is : ", saleAmountToSalary(saleAmount))
	} else if choice == 6 {
		fmt.Println("Enter a number to get sum of square numbers: ")
		fmt.Scanln(&numberToSumSquare)
		fmt.Println("The sum of square numbers is : ", sumSquareNum(numberToSumSquare))				
	} else {
		fmt.Println("You entered a wrong value !")
	}
}

func radiusToPerimeter(radius float64) float64 {
	var perimeter float64

	perimeter = 2 * PI * radius
	return perimeter
}

func number123ToAverage(number1, number2, number3 float64) float64 {

	average := (number1 + number2 + number3) / 3

	return average
}

func priceToTotalWithTaxes(price float64) float64 {
	var total float64;
	const TVQ float64 = 0.09975
	const TPS float64 = 0.05
	
	if price <  50 {
		total = price
	} else if price < 100 {
		total = (price * TVQ) + price
	} else {
		total = (price * TVQ) + (price * TPS) + price
	}

	return total
}

func numberThatIsBigger(number1 float64, number2 float64) float64 {
	var biggerOne float64
	if number1 > number2 {
		biggerOne = number1
	} else if number2 < number1 {
		biggerOne = number2
	} else {
		biggerOne = 0
	}

	return biggerOne
}

func saleAmountToSalary(saleAmount float64) (salary float64) {
	if saleAmount < 10000 {
		salary = saleAmount * 0.05
	} else if saleAmount < 100000 {
		salary = saleAmount * 0.1 + 1000
	} else if saleAmount >= 100000 {
		salary = saleAmount * 0.2 + 5000
	} else {
		salary = 0
	}

	return salary
}

func sumSquareNum(number int) (total int) {
	for nums:=1; nums <= number; nums++ {
		total += nums * nums
	}

	return total
}
