package main
import ("fmt")

func main() {
	var operation int
	var choice string
	var result float64 = 0
	var num float64
	var wantToContinue bool = true
	
	for wantToContinue == true {
		fmt.Println("Choose an operation :")
		fmt.Println("	1) Decimal to binary")
		fmt.Println("	2) Binary to decimal not signed")
		fmt.Println("	3) Binary to decimal signed")
		fmt.Println("")
		fmt.Scanln(&operation)
		fmt.Println("")
		fmt.Println("Provide a number")
		fmt.Scanln(&num)

		if operation == 1 {
			result = decToBin(num)
		} else if operation == 2 {
			result = 0
		} else {
			fmt.Println("You entered a wrong value !")
			break
		}

		fmt.Println("\nResult is :", result)
		fmt.Println("Do you want to continue ? (Y/n)")
		fmt.Scanln(&choice)
		if choice == "n" || choice == "no" || choice == "No" {
			wantToContinue = false
		} else {
			wantToContinue = true
		}
	}

	fmt.Println("\nThanks for using GoASCII !")
}

func decToBin(num float64) (result float64){
	
	result = num

	return result
}
