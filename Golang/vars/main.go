package main
import ("fmt")

// Declare global variables
var message1 string = "1) Hello World !" // could be in or out the func
var message2 = "2) Hello World !" // var type is inferred
var message3 string // var type inferred and value undefined
const PI float32 = 3.14 // is ferred but could be inferred
 // already declared, but now is set
func main() { 
	message3 = "3) Hello World !" // already declared, but now is set
	message4 := "4) Hello World !" // must be inside func
	var message5, message6 string = "5) Hello World !", "6) Hello World !"
	var ( // <== is a var. could be a const instead
		number1 int
		number2 int = 7
		message7 string = "7) Hello World !"
	)

	number3, message8 := 9, "8) Hello World !"

	// Print the vars
	fmt.Println("Messages : ")
	fmt.Println(message1)
	fmt.Println(message2)
	fmt.Println(message3)
	fmt.Println(message4)
	fmt.Println(message5)
	fmt.Println(message6)
	fmt.Println(message7)
	fmt.Println(message8)

	fmt.Println("\nNumbers : ")
	fmt.Println(number1)
	fmt.Println(number2)
	fmt.Println(number3)
	fmt.Println(PI)
	
}
