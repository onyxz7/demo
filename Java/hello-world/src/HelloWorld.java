public class HelloWorld implements IHelloWorld {
    public String msg;

    public HelloWorld() {
        this.msg = "Hello World!";
    }

    @Override
    public void sayHello() {
        System.out.println(this.msg);
    }
}
