import PySimpleGUI as sg 

# Define the color scheme
global_color_scheme = {
    'background': '#1C1C1C',
    'text': '#FFFFFF',
    'button': ('#FFFFFF', '#505050')
}

rightline_color_scheme = ('#FFFFFF', '#FF9500')

uperline_color_scheme = ('#000000','#d4d4d2')

# Set the theme colors
sg.theme_background_color(global_color_scheme['background'])
sg.theme_text_color(global_color_scheme['text'])
sg.theme_button_color(global_color_scheme['button'])

button_size = (6,3)
sg.set_options(font = 'Franklin 11', button_element_size = button_size)

layout = [
    [sg.Text(
        '', 
        background_color = global_color_scheme['background'], 
        font = 'Franklin 26', 
        justification = 'right', 
        expand_x = True, pad = (10,20), 
        key = '-OUTPUT-')
    ],
    [sg.Button('AC', size = button_size, button_color = uperline_color_scheme), sg.Button('±', size = button_size, button_color = uperline_color_scheme), sg.Button('%', size = button_size, button_color = uperline_color_scheme), sg.Button('÷', size = button_size, button_color = rightline_color_scheme)],
    [sg.Button('7', size = button_size), sg.Button('8', size = button_size), sg.Button('9', size = button_size), sg.Button('*', size = button_size, button_color = rightline_color_scheme)],
    [sg.Button('4', size = button_size), sg.Button('5', size = button_size), sg.Button('6', size = button_size), sg.Button('-', size = button_size, button_color = rightline_color_scheme)],
    [sg.Button('1', size = button_size), sg.Button('2', size = button_size), sg.Button('3', size = button_size), sg.Button('+', size = button_size, button_color = rightline_color_scheme)],
    [sg.Button('0', expand_x = True), sg.Button('.', size = button_size), sg.Button('=', size = button_size, button_color = rightline_color_scheme)],
]

app = sg.Window('Oz Calculator', layout)

current_num = []
full_operation = []

while True:
    event, values = app.read()
    if event == sg.WIN_CLOSED:
        break

    if event in ['0','1','2','3','4','5','6','7','8','9','.']:
        current_num.append(event)
        current_num_string = ''.join(current_num)
        try: 
            current_num_float = float(current_num_string)
        except ValueError:
            current_num_string = 'Not a number'
        app['-OUTPUT-'].update(current_num_string)

    if event in ['+','-','*','÷']:
        if event == '÷':
            event = '/'
        full_operation.append(''.join(current_num))
        current_num = []
        full_operation.append(event)
        app['-OUTPUT-'].update('')

    if event == '=':
        try:
            full_operation.append(''.join(current_num))
            full_operation_string = ' '.join(full_operation)
            result = eval(full_operation_string)
            app['-OUTPUT-'].update(result)
            full_operation = []
            current_num = []
        except SyntaxError:
            current_num_string = 'Press AC before a new operation'
            app['-OUTPUT-'].update(current_num_string)
            current_num = []
            full_operation =[]

    if event == 'AC': 
        current_num = []
        full_operation =[]
        app['-OUTPUT-'].update('')

app.close()