import PySimpleGUI as sg 

units = ['g to lb', 'lb to g', 'm to mil', 'mil to m']

layout = [
    [sg.Input(key = '-INPUT-'),sg.Combo(units, default_value=units[0], key = '-UNITS-')],
    [sg.Text('', key = '-RESULT-', visible = False)],
    [sg.Button('Convert', key = '-CONVERT-'), sg.Button('Close', key ='-CLOSE-')],
]

def convert(units, input):
    try:
        input_float = float(input)
        match units:
            case 'g to lb': result = f"{input_float / 453.59237} lb"
            case 'lb to g': result = f"{input_float * 453.59237} g"
            case 'm to mil': result = f"{input_float / 1609.344} lb"
            case 'mil to m': result = f"{input_float * 1609.344} g"
    except ValueError:
        result = 'You have entered a wrong value. Only decimal numbers are supported!'
    return result

app = sg.Window('Oz Converter', layout)

while True: 
    event, values = app.read()

    if event == sg.WIN_CLOSED or event == '-CLOSE-':
        break

    if event == '-CONVERT-':
        app['-RESULT-'].update(convert(units = values['-UNITS-'], input = values['-INPUT-']), visible = True)

app.close()