#!/bin/python

def calcTax(taxRate: int):
    return lambda price : price + price * (taxRate/100);

canadianCalcTax = calcTax(14)
frenchCalcTax = calcTax(10)

product_price = 5;

print(f"Price in Canada for {product_price} CAD : {canadianCalcTax(product_price)}")
print(f"Price in France for {product_price} EUR : {frenchCalcTax(product_price)}")

