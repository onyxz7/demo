fruits = ["apple", "orange", "lemon"]
print(f"The list fruits have {len(fruits)} items\nThose are : ")
for fruit in fruits:
    print(fruit)

print(f"\nChanging {fruits[1]} for pear")
fruits[1] = "pear"
[print(fruit) for fruit in fruits]

