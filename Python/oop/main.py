#!/bin/python

class Paper:
    def __init__(self, text: list[str]):
       self.__text = text
    def modifyText(self, newText: list[str]):
        self.__text = newText
    def modifyLine(self, lineNumber: int, newLine: str):
        self.__text[(lineNumber-1)] = newLine
    def getLinesNumber(self):
        return len(self.__text)
    def __str__(self):
        return str(self.__text)

class Book:
    def __init__(self, title: str, author: str, pages: list[Paper]):
        self.__title = title
        self.__author = author
        self.__pages = pages

page1 = Paper(["This is my first line", "This is my second line", "3rd line"])
print(f"Original page1 :\n{page1}\nIt has " + str(page1.getLinesNumber()) + " lines")
print("Changing the 2nd line...")
page1.modifyLine(2, "This is my new 2nd line")
print(f"New page1 : \n{page1}")
