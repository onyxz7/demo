#!/bin/python

import unittest
from main import aPLusB

class TestAPLusB(unittest.TestCase):
    def test_sum_positive_integers(self):
        self.assertEqual(aPLusB(1, 2), 3, "Should be 3")

    def test_sum_negative_integers(self):
        self.assertEqual(aPLusB(-1, -2), -3, "Should be -3")

    def test_sum_positive_and_negative_integer(self):
        self.assertEqual(aPLusB(-1, 2), 1, "Should be 1")

if __name__ == '__main__':
    unittest.main()

