import { defineConfig } from "vite";
import usePHP from "vite-plugin-php";

export default defineConfig({
  base: "./",
  plugins: [
    usePHP({
      entry: ["index.php"],
    }),
  ],
});
